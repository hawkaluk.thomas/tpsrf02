//Hawkaluk Thomas
//Fichier "Main.cpp"
//18/02/2021
#include <Arduino.h>
#include "SRF02.hpp" //inclusion de la lib SRF02
SRF02 srf(/* addrI2C=*/0x70, /* sda=*/D2, /* scl=*/D1);
int distance = 0;
void setup()
{
Serial.begin(9600); // start serial communication at 9600bps
}
void loop()
{
distance = srf.LireDistance();
String msg = (String)distance + " cm";
Serial.println(msg);
delay(200); // wait a bit since people have to read the output :)
}